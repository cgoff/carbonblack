#!/bin/python

# Copyright (c) 2018 Chris Goff cgoff1@fairview.org
#
# Permission to use, copy, modify, and distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.


# Pulls all Carbon Black Defense events for a system going back two years (default)
# Note that CbD will only let you query two weeks worth of data at a time.

from datetime import datetime, timedelta
import requests
import time
import urllib3

# Add your API token, API URL, and the system hostname you want to query events for.
api_token = ''
api_url = ''
system_hostname = ''

def api_grab(thehost, bucket, previous_two_weeks):
	""" Function to query the Carbon Black Defense API for all events in a time frame for a specific host """
	headers = {
    'X-Auth-Token': api_token,
	}

	params = (
    ('hostName', thehost),
    ('startTime', previous_two_weeks),
    ('endTime', bucket)
	)

	# Grab the REST API output and append it to a file--in this instance 'output.json'
	response = requests.get(api_url, headers=headers, params=params, verify=False)
	with open('output.json', 'a') as f:
		f.write(response.text)
	f.close()

def week_calc(num_weeks=104, days_composing_weeks=14):
	""" Function to calculate every two weeks for X weeks and return in RFC3339 format """
	bucket = datetime.now()
	previous_two_weeks = datetime.now() - timedelta(days=days_composing_weeks)

	while num_weeks:
		api_grab(system_hostname, bucket.strftime("%Y-%m-%d"), previous_two_weeks.strftime("%Y-%m-%d"))
		print("%s - %s" % (bucket.strftime("%Y-%m-%d"), previous_two_weeks.strftime("%Y-%m-%d")))
		bucket = previous_two_weeks
		num_weeks = num_weeks - 2
		previous_two_weeks = previous_two_weeks - timedelta(days=days_composing_weeks)
		time.sleep(30)


if __name__ == '__main__':
	urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
	week_calc()
